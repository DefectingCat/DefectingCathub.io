import RUASandpack from 'components/RUA/RUASandpack';
import Anchor from 'components/mdx/Anchor';
import Image from 'components/mdx/Image';
import Tab from 'components/RUA/tab';
import TabItem from 'components/RUA/tab/TabItem';
import RUACodeSandbox from 'components/RUA/RUACodeSandbox';

const components = {
  RUASandpack,
  a: Anchor,
  Image,
  Tab,
  TabItem,
  RUACodeSandbox,
};

export default components;
